// /*
// Programa creado por youtube.com/ElTallerDeTD
// 20/2/20
// https://github.com/imTDB
// */

// if (annyang) {

//     //Variable para almacenar las voces de nuestro sistema.
//     var voices;

//     //Inicializamos utter.
//     var utter = new SpeechSynthesisUtterance();
//     utter.rate = 1;
//     utter.pitch = 0.5;
//     utter.lang = 'es-AR';

//     //Cargamos las voces que tenemos en nuestro sistema y las mostarmos en un arreglo por consola.
//     window.speechSynthesis.onvoiceschanged = function () {
//         voices = window.speechSynthesis.getVoices();
//         console.log(voices);
//     };

//     //Definimos los comandos a utilizar.
//     var commands = {
//         'hola maria': function () {
//             utter.text = 'Hola usuario';
//             //Setea la voz que queremos usar en base a nuestra lista.
//             utter.voice = voices[2];
//             window.speechSynthesis.speak(utter);
//         },
//         'como estas': function () {
//             utter.text = 'Muy bien!';
//             utter.voice = voices[2];
//             window.speechSynthesis.speak(utter);
//         },
//         'hola': function () {
//             utter.text = 'hola, cual es tu nombre?';
//             utter.voice = voices[2];
//             window.speechSynthesis.speak(utter);
//             //Guarda el nombre que le decimos por voz.
//             annyang.addCallback('result', function (phrases) {
//                 //Imprime el nombre por consola.
//                 console.log("Nombre: ", phrases[0]);
//                 //Para el evento result.
//                 annyang.removeCallback('result');
//                 //Nos dice hola + el nombre que le digamos por voz.
//                 utter.text = 'Hola, ' + phrases[0];
//                 window.speechSynthesis.speak(utter);
//             });
//         },
//         //Array que devuelve aleatoriamente un elemento del array, en este caso un chiste.
//         'cuentame un chiste': function () {
//             chistes = ['Por qué las focas del circo miran siempre hacia arriba?   Porque es donde están los focos.',
//                 'Estas obsesionado con la comida!   No se a que te refieres croquetamente.',
//                 'Por que estás hablando con esas zapatillas?   Porque pone "converse"',
//                 'Buenos dias, me gustaria alquilar "Batman Forever".   No es posible, tiene que devolverla tomorrow.'
//             ];
//             utter.text = chistes[Math.floor(Math.random() * chistes.length)]
//             utter.voice = voices[2];
//             window.speechSynthesis.speak(utter);
//         }
//     };

//     //Esto nos sirve para ver que escucha el programa en tiempo real.
//     /*
//     annyang.addCallback('result', function(phrases) {
//       console.log("I think the user said: ", phrases[0]);
//       console.log("But then again, it could be any of the following: ", phrases);
//        });
//        */


//     //Sumamos todos los comandos a annyang.
//     annyang.addCommands(commands);

//     //Annyang comienza a escuchar.
//     annyang.start({ autoRestart: false, continuous: true });
// }

// Add a single command
var comandoHola = {
    indexes:["Hola","buenos días","holita"], // Decir alguna de estas palabras activara el comando
    action:function(){ // Acción a ejecutar cuando alguna palabra de los indices es reconocida
        artyom.say("Hola! como estás hoy?");
    }
};

artyom.addCommands(comandoHola); // Agregar comando

// Or add multiple commands at time
var myGroup = [
    {
        description:"Si mi base de datos contiene alguno del nombre dicho, hacer algo",
        smart:true, // Activar comando como un comando smart para poder usar comodines
        indexes:["Sabes quien es *","No se quien  *","Es * una buena persona"],
        // Ejecutar acción
        // i continene el indice que coincide con lo dicho en el array
        action:function(i,wildcard){
            var database = ["Carlos","Bruce","David","Joseph","Kenny"];

            //Si lo dicho, coincide con la tercera propiedad de los indices
            //es decir, "Es xxx una buena persona", haga X, de lo contrario Y
            if(i == 2){
                if(database.indexOf(wildcard.trim())){
                    artyom.say("Soy una máquina, nisiquiera se que es un sentimiento.");
                }else{
                    artyom.say("No se quien es " + wildcard + " y no se como demonios podría decir si es una buena persona o no.");
                }
            }else{
                if(database.indexOf(wildcard.trim())){
                    artyom.say("Por supuesto que se quien es "+ wildcard + ". Una muy buena persona a mi parecer.");
                }else{
                    artyom.say("Mi base de datos no es lo suficientemente amplia, no se quien es " + wildcard);
                }
            }
        }
    },
    {
        indexes:["Que hora es","Es muy tarde"],
        action:function(i){
            if(i == 0){
                UnaFuncionQueDiceElTiempo(new Date());
            }else if(i == 1){
                artyom.say("Nunca es tarde para hacer algo mi amigo!");
            }
        }
    }
];

artyom.addCommands(myGroup); 